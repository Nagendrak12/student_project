from django.shortcuts import render
from .models import Home
from .models import Studentinfo


# Create your views here.
def addHome(request):
    return render(request,'index.html')

def addStudent(request):
    return render(request,'student.html')

def addSec(request):
    return render(request,'gradeSec.html')

def viewData(request):
    obj=Home.objects.all()
    print(obj)
    return render(request,'databaseview/database.html',{'obj':obj})

def viewStd(request):
    obj1=Studentinfo.objects.all()
    print(obj1)
    return render(request,'databaseview/stdbase.html',{'obj1':obj1})