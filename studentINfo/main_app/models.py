from django.db import models

# Create your models here.
class Home(models.Model):
    gradename=models.CharField( max_length=50)
    subjectname=models.CharField( max_length=50)
    sectionname=models.CharField( max_length=50)
    
    
class Studentinfo(models.Model):
    name=models.CharField(max_length=50)
    enum=models.CharField(max_length=50)
    grade=models.CharField(max_length=50)
    sec=models.CharField(max_length=50)
    mail=models.CharField(max_length=50)
    mnum=models.CharField(max_length=50)
    img=models.ImageField(upload_to='photos/')
    
# class Devpyjp(models.Model):
#     title=models.CharField(max_length=120)
#     img=models.ImageField(upload to='photos/')