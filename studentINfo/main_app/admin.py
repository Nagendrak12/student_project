# from . models import Devpyjp
from django.contrib import admin
from .models import Home
from .models import Studentinfo
# Register your models here.

class Homeadmin(admin.ModelAdmin):
    list_display=('id','gradename','sectionname','subjectname')
    list_filter=('gradename','sectionname','subjectname')

class Studentinfoadmin(admin.ModelAdmin):
   list_display=('id','name','enum','grade','sec','mail','mnum')
   list_filter = ('name', 'enum', 'grade', 'sec', 'mail', 'mnum')



# admin.site.register(Devpyjp)


admin.site.register(Home,Homeadmin)
admin.site.register(Studentinfo,Studentinfoadmin)
